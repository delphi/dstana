# DELPHI Analysis framework source code

This project contains the DELPHI analysis framework software in Fortran 77

## Versions
The repository contains different versions. The naming convention for branches is YYMMDD where YYMMDD corresponds to the release date.

To checkout a release do
```
git clone https://:@gitlab.cern.ch:8443/delphi/dstana.git
cd dstana
git checkout YYMMDD
```

The master contains the latest prerelease version

## Usage
Examples can be found in
/cvmfs/delphi.cern.ch/sources/archive/examples.tgz
