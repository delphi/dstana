$ALIAS u_stack_trace = 'U_STACK_TRACE'
$ALIAS fflush = 'fflush'
      SUBROUTINE INITRP
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      ON EXTERNAL ERROR CALL TRAPEX
      ON INTERNAL ERROR CALL INTRAP
      ON REAL*4 DIV 0 CALL DIV0T4
      ON REAL*4 UNDERFLOW CALL UNFLT4
      ON REAL*4 OVERFLOW CALL OVFLT4
*      ON REAL*4 INEXACT CALL INEXT4
      ON REAL*4 ILLEGAL CALL ILLET4
      ON REAL*8 DIV 0 CALL DIV0T8
      ON REAL*8 UNDERFLOW CALL UNFLT8
      ON REAL*8 OVERFLOW CALL OVFLT8
*      ON REAL*8 INEXACT CALL INEXT8
      ON REAL*8 ILLEGAL CALL ILLET8
      ON INTEGER*2 DIV 0 CALL DIV0I2
      ON INTEGER*2 OVERFLOW CALL OVFLI2
      ON INTEGER*4 DIV 0 CALL DIV0I4
      ON INTEGER*4 OVERFLOW CALL OVFLI4
      FAULT=.FALSE.
      RETURN
      END
*
      SUBROUTINE TRAPEX(IERR,RES,FOP,SOP)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      CHARACTER*128 ERRMSG(100)
      DATA ERRMSG(29) /'DACOS(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(27) /'DASIN(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(28) /'ACOS(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(26) /'ASIN(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(3)(1:29) /'DATAN2(X,Y): X = Y = 0.0 OR X'/
      DATA ERRMSG(3)(30:)  /'= Y = INFINITY OR ANY OF X AND Y = NaN'/
      DATA ERRMSG(1)(1:29) /'ATAN2(X,Y): X = Y = 0.0 OR X '/
      DATA ERRMSG(1)(30:)  /'= Y = INFINITY OR ANY OF X AND Y = NaN'/
      DATA ERRMSG(25) /'DCOSH(X) OVERFLOW'/
      DATA ERRMSG(23) /'DSINH(X) OVERFLOW'/
      DATA ERRMSG(24) /'COSH(X) OVERFLOW'/
      DATA ERRMSG(22) /'SINH(X) OVERFLOW'/
      DATA ERRMSG(6) /'DEXP(X) OVERFLOW OR UNDERFLOW'/
      DATA ERRMSG(5) /'EXP(X) OVERFLOW OR UNDERFLOW'/
      DATA ERRMSG(9) /'CABS(X) OVERFLOW'/
      DATA ERRMSG(8) /'DLOG(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(21) /'DLOG10(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(7) /'ALOG(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(20) /'ALOG10(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(11) /'DSQRT(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(10) /'SQRT(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(19)(1:32) /'DTAN(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(19)(33:) /' OR DTAN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(15)(1:32) /'DSIN(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(15)(33:) /' OF DSIN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(17)(1:32) /'DCOS(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(17)(33:) /' OR DCOS(X): X = NaN OR INFINITY'/
      DATA ERRMSG(18)(1:31) /'TAN(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(18)(32:) /' OR TAN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(14) /'SIN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(16) /'COS(X): X = NaN OR INFINITY'/
      DATA ERRMSG(30) /'DATAN(X): X = NaN'/
      DATA ERRMSG(31) /'ATAN(X): X = NaN'/
      DATA ERRMSG(37)(1:31) /'DMOD(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(37)(32:) /'OR X = INFINITY OR Y = 0.0'/
      DATA ERRMSG(38)(1:31) /'AMOD(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(38)(32:) /'OR X = INFINITY OR Y = 0.0'/
      DATA ERRMSG(39) /'DTANH(X): X = NaN'/
      DATA ERRMSG(36) /'TANH(X): X = NaN'/
      DATA ERRMSG(35) /'ZABS(X) OVERFLOW OR ANY PART OF X = NaN'/
      DATA ERRMSG(34) /'DABS(X): X = NaN'/
      DATA ERRMSG(40) /'ABS(X): X = NaN'/
      DATA ERRMSG(41) /'AINT(X): X = NaN'/
      DATA ERRMSG(42) /'DINT(X) OR DDINT(X): X = NaN'/
      DATA ERRMSG(43)(1:31) /'REAL POWER FUNCTION: ARGUMENTS '/
      DATA ERRMSG(43)(32:) /'= NaN or ILLEGAL'/
      DATA ERRMSG(44)(1:36) /'COMPLEX POWER FUNCTION: ARGUMENT(S) '/
      DATA ERRMSG(44)(37:) /'= NaN or ILLEGAL'/
      DATA ERRMSG(45)(1:38) /'COMPLEX CONJUGATE FUNCTION: IMAGINARY '/
      DATA ERRMSG(45)(39:) /'= NaN'/
      DATA ERRMSG(46)(1:34) /'POSITIVE DIFFERENCE FUNCTION: ANY '/
      DATA ERRMSG(46)(35:70) /'ARGUMENT = NaN OR BOTH ARGUMENTS ARE'/
      DATA ERRMSG(46)(71:) /' INFINITIES WITH THE SAME SIGN.'/
      DATA ERRMSG(47) /'SIGN FUNCTION: ANY ARGUMENT = NaN'/
      DATA ERRMSG(48) /'COMPLEX SQRT: ANY PART OF THE ARGUMENT = NaN'/
      DATA ERRMSG(49)(1:39) /'DPROD(X,Y): ANY OF X AND Y = NaN, OR X '/
      DATA ERRMSG(49)(40:78) /'= 0.0 AND Y = INFINITY, OR X = INFINITY'/
      DATA ERRMSG(49)(79:) /' AND Y = 0.0'/
      DATA ERRMSG(51) /'IMAGINARY PART OF COMPLEX NUMBER = NaN'/
      DATA ERRMSG(80) /'QACOS(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(72) /'QACOSH(X): X < 1.0 OR X = NaN'/
      DATA ERRMSG(81) /'DASIN(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(82) /'QASINH(X): X = NaN'/
      DATA ERRMSG(83) /'QATAN(X): X = NaN'/
      DATA ERRMSG(84) /'QATAN2(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(73) /'QATANH(X): |x| > 1.0 OR X = NaN'/
      DATA ERRMSG(85) /'QCOS(X): |X| = INFINITY OR X = NaN'/
      DATA ERRMSG(86) /'QCOSH(X): X = NaN OR QCOSH(X) OVERFLOW'/
      DATA ERRMSG(74) /'QEXP(X): X = NaN OR QEXP(X) OVERFLOW'/
      DATA ERRMSG(76) /'QLOG(X): X < 0.0 OR = NaN'/
      DATA ERRMSG(77) /'QLOG10(X): X < 0.0 OR = NaN'/
      DATA ERRMSG(75)(1:35) /'QMOD(X,Y): ANY OF X AND Y = NaN, OR'/
      DATA ERRMSG(75)(36:) /' X = INFINITY, OR Y = 0.0'/
      DATA ERRMSG(92) /'QSIGN(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(88) /'QSIN(X): |X| = INFINITY OR X = NaN'/
      DATA ERRMSG(89) /'QSINH(X) QSINH(X): X = NaN OR OVERFLOW'/
      DATA ERRMSG(78) /'QSQRT(X): X < 0.0 OR = NaN'/
      DATA ERRMSG(90) /'QTAN(X): |X| = INFINITY OR X = NaN'/
      DATA ERRMSG(91) /'QTANH(X): X = NaN'/
      DATA ERRMSG(79)(1:35) /'QTOQ(X,Y): ANY OF X AND Y = NaN, OR'/
      DATA ERRMSG(79)(36:62) /' X < 0.0 AND Y = NONINTEGER'/
      DATA ERRMSG(79)(63:) /' OR QINT(X): X = NaN'/
      DATA ERRMSG(54) /'ITOI:Base=0 and Power < 0'/
      DATA ERRMSG(55) /'RTOI:Base=0 and Power < 0'/
      DATA ERRMSG(56) /'RTOR:Base=0 and Power < 0'/
      DATA ERRMSG(59) /'LTOL:Base=0 and Power < 0'/
      DATA ERRMSG(60) /'CTOI:Base=0 and Power < 0'/
      DATA ERRMSG(67) /'DTOI:Base=0 and Power < 0'/
      DATA ERRMSG(68) /'DTOD:Base=0 and Power < 0'/
      DATA ERRMSG(69) /'QTOQ:Base=0 and Power < 0'/
      DATA ERRMSG(61) /'READ UNEXPECTED CHARACTER'/
      DATA ERRMSG(62) /'VALUE OUT OF RANGE IN NUMERIC READ'/
      DOUBLE PRECISION DFOP,DSOP
      EQUIVALENCE (RRES,IRES)
      EQUIVALENCE (RFOP,IFOP)
      EQUIVALENCE (RSOP,ISOP)
      EQUIVALENCE (RFOP,DFOP)
      EQUIVALENCE (RSOP,DSOP)
      FAULT=.TRUE.
      RRES=RES
      RFOP=FOP
      RSOP=SOP
      WRITE(*,*) ' ---------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR BASIC EXTERNAL FUNCTION CALLED'
      WRITE(*,*) ERRMSG(IERR)
      WRITE(*,*) ' First operand  (Integer/Float/Double) :'
     &,            IFOP,'/',RFOP,'/',DFOP
      WRITE(*,*) ' Second operand (if any) (Integer/Float/Double):'
     &,            ISOP,'/',RSOP,'/',DSOP
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      RES=0.
      RETURN
      END
      SUBROUTINE INTRAP(IERR,RES,CWORD)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      CHARACTER*128 ERRMSG(100)
      DATA ERRMSG(29) /'DACOS(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(27) /'DASIN(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(28) /'ACOS(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(26) /'ASIN(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(3)(1:29) /'DATAN2(X,Y): X = Y = 0.0 OR X'/
      DATA ERRMSG(3)(30:)  /'= Y = INFINITY OR ANY OF X AND Y = NaN'/
      DATA ERRMSG(1)(1:29) /'ATAN2(X,Y): X = Y = 0.0 OR X '/
      DATA ERRMSG(1)(30:)  /'= Y = INFINITY OR ANY OF X AND Y = NaN'/
      DATA ERRMSG(25) /'DCOSH(X) OVERFLOW'/
      DATA ERRMSG(23) /'DSINH(X) OVERFLOW'/
      DATA ERRMSG(24) /'COSH(X) OVERFLOW'/
      DATA ERRMSG(22) /'SINH(X) OVERFLOW'/
      DATA ERRMSG(6) /'DEXP(X) OVERFLOW OR UNDERFLOW'/
      DATA ERRMSG(5) /'EXP(X) OVERFLOW OR UNDERFLOW'/
      DATA ERRMSG(9) /'CABS(X) OVERFLOW'/
      DATA ERRMSG(8) /'DLOG(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(21) /'DLOG10(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(7) /'ALOG(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(20) /'ALOG10(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(11) /'DSQRT(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(10) /'SQRT(X): X < 0.0 OR X = NaN'/
      DATA ERRMSG(19)(1:32) /'DTAN(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(19)(33:) /' OR DTAN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(15)(1:32) /'DSIN(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(15)(33:) /' OF DSIN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(17)(1:32) /'DCOS(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(17)(33:) /' OR DCOS(X): X = NaN OR INFINITY'/
      DATA ERRMSG(18)(1:31) /'TAN(X): TOTAL LOSS OF PRECISION'/
      DATA ERRMSG(18)(32:) /' OR TAN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(14) /'SIN(X): X = NaN OR INFINITY'/
      DATA ERRMSG(16) /'COS(X): X = NaN OR INFINITY'/
      DATA ERRMSG(30) /'DATAN(X): X = NaN'/
      DATA ERRMSG(31) /'ATAN(X): X = NaN'/
      DATA ERRMSG(37)(1:31) /'DMOD(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(37)(32:) /'OR X = INFINITY OR Y = 0.0'/
      DATA ERRMSG(38)(1:31) /'AMOD(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(38)(32:) /'OR X = INFINITY OR Y = 0.0'/
      DATA ERRMSG(39) /'DTANH(X): X = NaN'/
      DATA ERRMSG(36) /'TANH(X): X = NaN'/
      DATA ERRMSG(35) /'ZABS(X) OVERFLOW OR ANY PART OF X = NaN'/
      DATA ERRMSG(34) /'DABS(X): X = NaN'/
      DATA ERRMSG(40) /'ABS(X): X = NaN'/
      DATA ERRMSG(41) /'AINT(X): X = NaN'/
      DATA ERRMSG(42) /'DINT(X) OR DDINT(X): X = NaN'/
      DATA ERRMSG(43)(1:31) /'REAL POWER FUNCTION: ARGUMENTS '/
      DATA ERRMSG(43)(32:) /'= NaN or ILLEGAL'/
      DATA ERRMSG(44)(1:36) /'COMPLEX POWER FUNCTION: ARGUMENT(S) '/
      DATA ERRMSG(44)(37:) /'= NaN or ILLEGAL'/
      DATA ERRMSG(45)(1:38) /'COMPLEX CONJUGATE FUNCTION: IMAGINARY '/
      DATA ERRMSG(45)(39:) /'= NaN'/
      DATA ERRMSG(46)(1:34) /'POSITIVE DIFFERENCE FUNCTION: ANY '/
      DATA ERRMSG(46)(35:70) /'ARGUMENT = NaN OR BOTH ARGUMENTS ARE'/
      DATA ERRMSG(46)(71:) /' INFINITIES WITH THE SAME SIGN.'/
      DATA ERRMSG(47) /'SIGN FUNCTION: ANY ARGUMENT = NaN'/
      DATA ERRMSG(48) /'COMPLEX SQRT: ANY PART OF THE ARGUMENT = NaN'/
      DATA ERRMSG(49)(1:39) /'DPROD(X,Y): ANY OF X AND Y = NaN, OR X '/
      DATA ERRMSG(49)(40:78) /'= 0.0 AND Y = INFINITY, OR X = INFINITY'/
      DATA ERRMSG(49)(79:) /' AND Y = 0.0'/
      DATA ERRMSG(51) /'IMAGINARY PART OF COMPLEX NUMBER = NaN'/
      DATA ERRMSG(80) /'QACOS(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(72) /'QACOSH(X): X < 1.0 OR X = NaN'/
      DATA ERRMSG(81) /'DASIN(X): |X| > 1.0 OR X = NaN'/
      DATA ERRMSG(82) /'QASINH(X): X = NaN'/
      DATA ERRMSG(83) /'QATAN(X): X = NaN'/
      DATA ERRMSG(84) /'QATAN2(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(73) /'QATANH(X): |x| > 1.0 OR X = NaN'/
      DATA ERRMSG(85) /'QCOS(X): |X| = INFINITY OR X = NaN'/
      DATA ERRMSG(86) /'QCOSH(X): X = NaN OR QCOSH(X) OVERFLOW'/
      DATA ERRMSG(74) /'QEXP(X): X = NaN OR QEXP(X) OVERFLOW'/
      DATA ERRMSG(76) /'QLOG(X): X < 0.0 OR = NaN'/
      DATA ERRMSG(77) /'QLOG10(X): X < 0.0 OR = NaN'/
      DATA ERRMSG(75)(1:35) /'QMOD(X,Y): ANY OF X AND Y = NaN, OR'/
      DATA ERRMSG(75)(36:) /' X = INFINITY, OR Y = 0.0'/
      DATA ERRMSG(92) /'QSIGN(X,Y): ANY OF X AND Y = NaN'/
      DATA ERRMSG(88) /'QSIN(X): |X| = INFINITY OR X = NaN'/
      DATA ERRMSG(89) /'QSINH(X) QSINH(X): X = NaN OR OVERFLOW'/
      DATA ERRMSG(78) /'QSQRT(X): X < 0.0 OR = NaN'/
      DATA ERRMSG(90) /'QTAN(X): |X| = INFINITY OR X = NaN'/
      DATA ERRMSG(91) /'QTANH(X): X = NaN'/
      DATA ERRMSG(79)(1:35) /'QTOQ(X,Y): ANY OF X AND Y = NaN, OR'/
      DATA ERRMSG(79)(36:62) /' X < 0.0 AND Y = NONINTEGER'/
      DATA ERRMSG(79)(63:) /' OR QINT(X): X = NaN'/
      DATA ERRMSG(54) /'ITOI:Base=0 and Power < 0'/
      DATA ERRMSG(55) /'RTOI:Base=0 and Power < 0'/
      DATA ERRMSG(56) /'RTOR:Base=0 and Power < 0'/
      DATA ERRMSG(59) /'LTOL:Base=0 and Power < 0'/
      DATA ERRMSG(60) /'CTOI:Base=0 and Power < 0'/
      DATA ERRMSG(67) /'DTOI:Base=0 and Power < 0'/
      DATA ERRMSG(68) /'DTOD:Base=0 and Power < 0'/
      DATA ERRMSG(69) /'QTOQ:Base=0 and Power < 0'/
      DATA ERRMSG(61) /'READ UNEXPECTED CHARACTER'/
      DATA ERRMSG(62) /'VALUE OUT OF RANGE IN NUMERIC READ'/
      CHARACTER*(*) CWORD
      CHARACTER*4 WORD
      DOUBLE PRECISION DFOP
      EQUIVALENCE (RRES,IRES)
      EQUIVALENCE (RFOP,IFOP)
      EQUIVALENCE (RFOP,DFOP)
      EQUIVALENCE (RFOP,WORD)
      FAULT=.TRUE.
      WORD=CWORD
      RFOP=FOP
      WRITE(*,*) ' ---------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR BASIC INTERNAL FUNCTION CALLED'
      WRITE(*,*) ERRMSG(IERR)
      IF (IERR.EQ.61.OR.IERR.EQ.62) THEN
        WRITE(*,*) ' Operand:',CWORD
      ELSE
        WRITE(*,*) ' Operand  (Integer/Float/Double) :'
        WRITE(*,*) IFOP,'/',RFOP,'/',DFOP
      ENDIF
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      RES=0.
      RETURN
      END
*
      SUBROUTINE DIV0T4(ARG)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*4 DIVISION BY 0 CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE UNFLT4(ARG)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*4 UNDERFLOW CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE OVFLT4(ARG)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*4 OVERFLOW CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE INEXT4(ARG)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*4 INEXACT CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE ILLET4(ARG)
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*4 ILLEGAL CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE DIV0T8(ARG)
      REAL*8 ARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*8 DIVISION BY 0 CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE UNFLT8(ARG)
      REAL*8 ARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*8 UNDERFLOW CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE OVFLT8(ARG)
      REAL*8 ARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*8 OVERFLOW CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE INEXT8(ARG)
      REAL*8 ARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*8 INEXACT CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
*
      SUBROUTINE ILLET8(ARG)
      REAL*8 ARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR REAL*8 ILLEGAL CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      ARG=0.0
      RETURN
      END
* 
      SUBROUTINE DIV0I2(IARG)
      INTEGER*2 IARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR INTEGER*2 DIVISION BY 0 CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      IARG=0.0
      RETURN
      END
*	
      SUBROUTINE OVFLI2(IARG)
      INTEGER*2 IARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR INTEGER*2 OVERFLOW CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      IARG=0.0
      RETURN
      END
* 
      SUBROUTINE DIV0I4(IARG)
      INTEGER IARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR INTEGER*4 DIVISION BY 0 CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      IARG=0.0
      RETURN
      END
*	
      SUBROUTINE OVFLI4(IARG)
      INTEGER IARG
      LOGICAL FAULT
      COMMON /FAULTS/ FAULT
      FAULT=.TRUE.
      WRITE(*,*) ' -----------------------------------------------'
      WRITE(*,*) ' ERROR TRAP FOR INTEGER*4 OVERFLOW CALLED'
      WRITE(*,*) ' Traceback to standard error output'
      CALL FFLUSH(%VAL(0))
      CALL U_STACK_TRACE
      WRITE(*,*) ' ------------------------------------------------'
      IARG=0.0
      RETURN
      END



