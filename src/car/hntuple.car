+TITLE.
HNTUPLE    28 Jan 93   Simplified n-tuple access routines
+PATCH, HNDOC.
N-tuples consist of a fixed number of ITEMS of data (eg. momentum, mass)
and a variable number of ENTRIES (eg. one for each track).
 
These routines provide a convenient interface to the HBOOK n-tuple
calls, by taking care of directory switching, speeding up the reading of
many entries at a time, combining the file opening and n-tuple booking
into a single routine, and handling correctly the closing of the files
at the end.
 
On the IBM HNMAKE and HNOPEN allow the file to be referenced by FILEDEF
instead of filename if desired (disk files only; the file
characteristics specified on the FILEDEF are ignored). This allows
n-tuples to be accessed directly on a STAGEing disk.
 
The KEEP, HNCLUN, is in VDKEEP CARDS.
 
NB. While the HNTUPLE routines work with the new-style (1993) ntuples,
    many of their features, such as non-real variables, are not
    available through HNTUPLE.
+PATCH, HNTUPLE.
+DECK, HNOPEN.
      SUBROUTINE HNOPEN (LUN, FNAME, CHOPT, LUNER, ID,
     &                   NENT, NTAGS, IERR)
      ENTRY      HNMAKE (LUN, FNAME, CHOPT, LUNER, ID,
     &                   TITLE, NTAGS, TAGS, IERR)
***********************************************************************
*
*     Input:
*       LUN     Logical Unit Number to use.
*       FNAME   File name to open. If already open, then this must
*               be identical to the previous file, or blank.
*               On the IBM, if specified as ' ' or '/', then the
*               filespec is determined from a FILEDEF associated with
*               LUN (ie. FTnnnFii, nnn=LUN, ii=01; if the file is closed
*               and reopened, then ii is increased by one). If it is
*               specified as '/NAME', then the FILEDEF NAME will be
*               used (note this is opposite to normal VFORT usage).
*               On the VAX the logical name associated with LUN is used.
*               The actual filename can be obtained using INQUIRE.
*       CHOPT   'N'      open new file                  (for HNMAKE)
*               'U'      open existing file for update  (for HNOPEN)
*               ' '      open file in readonly mode     (for HNOPEN)
*       LUNER   Unit for error messages (0=no messages). If negative,
*               then its ABSolute value will be used and debugging will
*               be enabled.
*       ID      N-tuple identifier (unique within this file)
*       TITLE   N-tuple title.                          (for HNMAKE)
*       NTAGS   Number of items in n-tuple       (output for HNOPEN)
*               If this is zero, then a new-style n-tuple will be
*               created with HBNT, rather than HBOOKN, and TAGS is not
*               used. It is then up to the caller to call HBNAME and
*               HBNAMC after HNCDIR.
*       TAGS    Names of each column of the n-tuple     (for HNMAKE)
*     Output:
*       NENT    #entries already stored in n-tuple      (for HNOPEN)
*       IERR    Error number (0=OK)
*
*     More than one file may be opened at a time (different LUNs).
*
*     If necessary TITLE and TAGS make be retrieved for an already-open
*     file with HNPARM.
*
*      1 Nov 91         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
+CDE, HNCLUN.
      INTEGER       LUN, LUNER, ID, NTAGS, NENT, IERR
      CHARACTER*(*) FNAME, TITLE, CHOPT
+SELF, IF=IBMVM.
      CHARACTER*(*) TAGS(NTAGS)
+SELF, IF=VAXVMS,ALPHAVMS.
*     The VAX will complain if the array size is given as zero,
*     unless we do the following.
*     For some crazy reason, the Alpha does not have MAX declared.
      INTEGER   MAX0
      INTRINSIC MAX0
      CHARACTER*(*) TAGS(MAX0(NTAGS,1))
+SELF, IF=-IBMVM, IF=-VAXVMS, IF=-ALPHAVMS.
      CHARACTER*(*) TAGS(*)
+SELF.
*
      INTEGER       IQUEST(100)
      COMMON/QUEST/ IQUEST
*
      CHARACTER*10  OCHOPT
      CHARACTER*255 FILE, LINE
      INTEGER       IPOS, LCHOPT, LFILE, I, NLINE, LLINE, IBUF, IRC
      LOGICAL       TAGOUT, WRMODE, NEWNT, FOUND
      INTEGER       KILROY
      DATA          KILROY  /0/
*
      INTEGER       LENOCC
***********************************************************************
      IF (KILROY.EQ.0) THEN
        CALL VZERO (IOPEN, MAXLUN)
        CALL VZERO (FSEQ,  MAXLUN)
        KILROY= 1
      ENDIF
*
      DEBUG= (LUNER.LT.0)
      LUNERR= ABS (LUNER)
*
      IF (LUN.LE.0 .OR. LUN.GT.MAXLUN) THEN
        IF (LUNERR.NE.0)
     &    WRITE (LUNERR,*) 'HNOPEN: invalid LUN:', LUN
        IERR= 99
        RETURN
      ENDIF
*
*     Save user's HBOOK/RZ directory.
      CALL HSAVCD
*
      NEWNT= (INDEX (CHOPT, 'N').GT.0)
      WRMODE= (NEWNT .OR. INDEX (CHOPT, 'U').GT.0)
*
*     Check whether file is already open.
      IF (IOPEN(LUN).LE.0) THEN
*
        FILE= FNAME
        LFILE= LEN (FNAME)
        LUNFIL(LUN)= FNAME
*
*       Make directory name in the form LUNnn.
        IF (LUN.LE.9) THEN
          WRITE (DIR(LUN), '(A,I1)') 'LUN', LUN
        ELSE
          WRITE (DIR(LUN), '(A,I2)') 'LUN', LUN
        ENDIF
*
        FILE= FNAME
        LFILE= LENOCC (FNAME)
+SELF, IF=VAXVMS,ALPHAVMS,IBMVM.
*       Unfortunately HROPEN forces us to specify a filename.
*       Since it is sometimes convenient to specify just the unit
*       number, we convert unit number to filename.
        IF (FNAME.EQ.'/') THEN
          FILE= ' '
          LFILE= 0
        ELSEIF (FNAME(1:1).EQ.'/') THEN
          FILE= FNAME(2:)
          LFILE= LENOCC (FNAME(2:))
        ENDIF
+SELF, IF=VAXVMS,ALPHAVMS.
        IF (FILE.EQ.' ') THEN
          WRITE (FILE,'(A,I3.3)') 'FOR', LUN
          LFILE= 6
        ENDIF
+SELF, IF=IBMVM.
*       We look up a FILEDEF if the filespec is blank, or is a single
*       word beginning with "/". The "/" has already been removed from
*       FILE, so if this is "/fn ft fm", then we are OK for when HROPEN
*       adds on the "/" again.
        IF (FNAME.EQ.' ' .OR.
     &      (FNAME(1:1).EQ.'/' .AND. INDEX (FILE, ' ')-1.EQ.LFILE)) THEN
          FOUND= .FALSE.
          IF (FILE.EQ.' ') THEN
            FSEQ(LUN)= FSEQ(LUN) + 1
            IF (FSEQ(LUN).LT.1) FSEQ(LUN)= 1
            WRITE (FILE,'(A,I2.2,A,I3.3)') 'FT', LUN, 'F', FSEQ(LUN)
            LFILE= 8
          ENDIF
          CALL VMCMS ('MAKEBUF', IBUF)
          CALL VMCMS ('QFILEDEF '//FILE(1:LFILE)//' (STACK', IRC)
          CALL VMCMS ('SENTRIES', NLINE)
CCC       IF (DEBUG)
CCC  &      WRITE (LUNERR,*) 'QFILEDEF ', FILE(1:LFILE), ' returned',
CCC  &                       NLINE, ' lines and RC=', IRC
          IF (IRC.NE.0) GOTO 15
          DO 10 I= 1, NLINE
            CALL VMRTRM (LINE, LLINE)
CCC         IF (DEBUG) WRITE (LUNERR,*) LINE(1:LLINE)
            IF (LLINE.LT.LEN (LINE)) LINE(LLINE+1:)= ' '
            IF (LINE.EQ.' ' .OR. LINE(1:1).EQ.'*') GOTO 10
            IF (LINE(1:14+LFILE).EQ.
     &          'FILEDEF '//FILE(1:LFILE)//' DISK ') THEN
              IPOS= INDEX (LINE(15+LFILE:LLINE), '(')
              IF (IPOS.GE.6) THEN
                FILE= LINE(15+LFILE:14+LFILE+IPOS-2)
                LFILE= IPOS - 2
                FOUND= .TRUE.
              ENDIF
            ENDIF
   10     CONTINUE
   15     CONTINUE
          CALL VMCMS ('DROPBUF', IBUF)
          IF (.NOT.FOUND) THEN
            IF (LUNERR.NE.0)
     &          WRITE (LUNERR,*) 'HNOPEN: no FILEDEF ', FILE(1:8)
            IERR= 97
            GOTO 999
          ENDIF
        ENDIF
+SELF.
*
*       Open file. This also sets HBOOK and RZ directories to //LUNnn.
*       In order to be able to write files >4000 blocks (=4000 records
*       with LREC=1024 words as here), we need to create them with
*       increased quota. A quota of 55000 means a maximum size of 215Mb.
*       We could make things more efficient if we set LREC=4096, but
*       then we would have to specify this whenever we opened the file
*       in PAW).
        IF (NEWNT) THEN
          IQUEST(10)= 55000
          OCHOPT= CHOPT// 'Q'
          LCHOPT= LEN (CHOPT) + 1
        ELSE
          OCHOPT= CHOPT
          LCHOPT= LEN (CHOPT)
        ENDIF
        IF (DEBUG)
     &    WRITE (LUNERR,*) 'HROPEN "', FILE(1:LFILE),
     &                     '" CHOPT= "', OCHOPT(1:LCHOPT), '"'
        CALL HROPEN (LUN, DIR(LUN), FILE(1:LFILE), OCHOPT(1:LCHOPT),
     &               1024, IERR)
        IF (IERR.NE.0) THEN
          IF (LUNERR.NE.0)
     &        WRITE (LUNERR,*) 'HNOPEN: error', IERR,
     &                         ' in HROPEN opening ', FILE(1:LFILE)
          GOTO 999
        ENDIF
*
*       Create and set HBOOK directory //PAWC/LUNnn.
        CALL HCDIR ('//PAWC', ' ')
        CALL HMDIR (DIR(LUN), 'S')
*
*       The LUN is now valid for calls to HNGET etc.
        IF (WRMODE) THEN
          IOPEN(LUN)= 2
        ELSE
          IOPEN(LUN)= 1
        ENDIF
 
      ELSE      ! File is already open
 
        IF (FNAME.NE.' ' .AND. FNAME.NE.'/' .AND.
     &      FNAME(1:MIN(LEN(FNAME),LEN(LUNFIL(1)))).NE.LUNFIL(LUN)) THEN
          IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*)
     &            'HNOPEN: Cannot open ', FNAME, ': LUN', LUN,
     &            ' in use for ', LUNFIL(LUN)(1:LENOCC(LUNFIL(LUN)))
          IERR= 98
          GOTO 999
        ELSEIF (WRMODE .AND. IOPEN(LUN).NE.2) THEN
          IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNOPEN: Cannot use LUN', LUN,
     &                       ' for output: it is already open read-only'
          IERR= 98
          GOTO 999
        ENDIF
        CALL HCDIR ('//'      // DIR(LUN), ' ')
        CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
 
      ENDIF
*
      IF (NEWNT) THEN
*       Book new n-tuple.
        IF (NTAGS.GE.1) THEN
          CALL HBOOKN (ID, TITLE, NTAGS, DIR(LUN), 5000, TAGS)
+SELF, IF=-OLDHBOOK.
        ELSE
          CALL HCDIR ('//'// DIR(LUN), ' ')
          CALL HBNT (ID, TITLE, ' ')
+SELF.
        ENDIF
      ELSE
*       Find out current length and number of items if the file
*       already exists.
        CALL HGNPRM (ID, NTAGS, NENT, IERR)
        IF (IERR.NE.0) THEN
          IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNOPEN: error', IERR, ' in HGNPAR'
          GOTO 999
        ENDIF
      ENDIF
*
      IERR= 0
*
  999 CONTINUE
      CALL HRESCD
      RETURN
      END
+DECK, HNCDIR.
      SUBROUTINE HNCDIR (LUN, IERR)
***********************************************************************
*
*     HNCDIR
*
*     Change to Zebra directory associated with unit LUN. Use HRESCD
*     to return to the previous directory.
*
*       LUN     Unit number used in previous HNOPEN
*      *IERR    Error condition (0=OK, 99=bad LUN)
*
*     26 Mar 93         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
+CDE, HNCLUN.
      INTEGER LUN, IERR
***********************************************************************
      IERR= 0
*
*     Check LUN is valid.
      IF (LUN.LE.0 .OR. LUN.GT.MAXLUN) THEN
        IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNCDIR: invalid LUN:', LUN
        IERR= 99
        RETURN
      ENDIF
*
*     Save user's HBOOK/RZ directory.
      CALL HSAVCD
*
*     Set HBOOK/RZ directory.
      CALL HCDIR ('//'      // DIR(LUN), ' ')
      CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
*
      RETURN
      END
+DECK, HNPARM.
      SUBROUTINE HNPARM (LUN, ID, MXV,
     &                   NV, NENT, TAGS, RLO, RHI, TITLE, IERR)
***********************************************************************
*
*     HNPARM
*
*     Read information about the n-tuple.
*
*       LUN     Unit number used in previous HNOPEN
*       ID      N-tuple identifier
*       MXV     Size of the TAGS, RLO, and RHI arrays
*      *NV      Number of items in the n-tuple
*      *NENT    Number of entries in the n-tuple
*      *TAGS    N-tuple tags
*      *RLO     Minimum value for each item
*      *RHI     Maximum value for each item
*      *TITLE   N-tuple title
*      *IERR    Error condition (0=OK; 1=NV>MXV)
*
*     26 Nov 91         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
+CDE, HNCLUN.
      INTEGER LUN, ID, MXV, NV, NENT, IERR
      CHARACTER*(*) TITLE
+SELF, IF=IBMVM.
      CHARACTER*(*) TAGS(MXV)
      REAL    RLO(MXV), RHI(MXV)
+SELF, IF=VAXVMS,ALPHAVMS.
*     The VAX will complain if the array size is given as zero,
*     unless we do the following.
*     For some crazy reason, the Alpha does not have MAX declared.
      INTEGER   MAX0
      INTRINSIC MAX0
      CHARACTER*(*) TAGS(MAX0(MXV,1))
      REAL    RLO(MAX0(MXV,1)), RHI(MAX0(MXV,1))
+SELF, IF=-IBMVM, IF=-VAXVMS, IF=-ALPHAVMS.
      CHARACTER*(*) TAGS(*)
      REAL    RLO(*), RHI(*)
+SELF.
*
      INTEGER IENT, NV1
***********************************************************************
      IERR= 0
      CALL VZERO (RLO, MXV)
      CALL VZERO (RHI, MXV)
      NENT= 0
      NV= 0
      TITLE= ' '
      DO 10 IENT= 1, MXV
        TAGS(IENT)= ' '
   10 CONTINUE
*
*     Check LUN is valid.
      IF (LUN.LE.0 .OR. LUN.GT.MAXLUN) THEN
        IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNPARM: invalid LUN:', LUN
        IERR= 99
        RETURN
      ENDIF
*
*     Save user's HBOOK/RZ directory.
      CALL HSAVCD
*
*     Set HBOOK/RZ directory.
      CALL HCDIR ('//'      // DIR(LUN), ' ')
      CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
*
      CALL HGNPRM (ID, NV1, NENT, IERR)
      IF (IERR.NE.0) THEN
        IF (LUNERR.NE.0)
     &    WRITE (LUNERR,*) 'HNPARM: error', IERR,
     &                     ' in HGNPAR for n-tuple ID', ID
        GOTO 999
      ENDIF
*
*     Get n-tuple parameters.
      NV= MXV
      CALL HGIVEN (ID, TITLE, NV, TAGS, RLO, RHI)
*
      IF (NV.NE.NV1) THEN
        IF (LUNERR.NE.0)
     &    WRITE (LUNERR,*) 'HNPARM: HGNPAR gives NV=', NV1,
     &                     ', but HGIVEN gives NV=', NV,
     &                     ' for n-tuple ID', ID
        IERR= -99
      ELSEIF (NV.GT.MXV) THEN
        IF (LUNERR.NE.0 .AND. MXV.GT.1)
     &    WRITE (LUNERR,*) 'HNPARM: n-tuple ID', ID,
     &                     ' has', NV, ' items; maximum is', MXV
        IERR= 1
      ENDIF
*
  999 CONTINUE
      CALL HRESCD
      RETURN
      END
+DECK, HNGET.
      SUBROUTINE HNGET (LUN, ID, IENT1, IENT2, MXV, DAT, NDAT, IERR)
***********************************************************************
*
*     HNGET
*
*     Read multiple events from the n-tuple.
*
*       LUN     Unit number used in previous HNOPEN
*       ID      N-tuple identifier
*       IENT1   First entry within n-tuple of data to read
*       IENT2   Last  entry within n-tuple of data to read
*       MXV     Width (1st dimension) of DAT array.
*      *DAT     Data in array (MXV,*)
*      *NDAT    Number of events written into DAT.
*      *IERR    Error condition (0=OK, -1=(IENT2.GT.max))
*
*      1 Nov 91         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
+CDE, HNCLUN.
      INTEGER LUN, ID, IENT1, IENT2, IERR, MXV, NDAT
+SELF, IF=IBMVM.
      REAL    DAT(MXV,IENT1:IENT2)
+SELF, IF=VAXVMS,ALPHAVMS.
*     The VAX will complain if the array size is given as zero,
*     unless we do the following.
*     For some crazy reason, the Alpha does not have MAX declared.
      INTEGER   MAX0
      INTRINSIC MAX0
      REAL    DAT(MXV,IENT1:MAX0(IENT2,IENT1))
+SELF, IF=-IBMVM, IF=-VAXVMS, IF=-ALPHAVMS.
      REAL    DAT(MXV,IENT1:*)
+SELF.
*
      INTEGER IENT, NENT, NV
***********************************************************************
      IERR= 0
      NDAT= 0
*
      IF (IENT2.LT.IENT1) RETURN
      CALL VZERO (DAT, MXV*(IENT2-IENT1+1))
*
*     Check LUN is valid.
      IF (LUN.LE.0 .OR. LUN.GT.MAXLUN) THEN
        IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNGET: invalid LUN:', LUN
        IERR= 99
        RETURN
      ENDIF
*
*     Save user's HBOOK/RZ directory.
      CALL HSAVCD
*
*     Set HBOOK/RZ directory.
      CALL HCDIR ('//'      // DIR(LUN), ' ')
      CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
*
      CALL HGNPRM (ID, NV, NENT, IERR)
      IF (IERR.NE.0) THEN
        IF (LUNERR.NE.0)
     &    WRITE (LUNERR,*) 'HNGET: error', IERR,
     &                     ' in HGNPAR for n-tuple ID', ID
        GOTO 999
      ENDIF
      IF (NV.GT.MXV) THEN
        IF (LUNERR.NE.0)
     &    WRITE (LUNERR,*) 'HNGET: n-tuple ID', ID, ' width', NV,
     &                     ' larger than maximum of', MXV
        IERR= 96
        GOTO 999
      ENDIF
      DO 10 IENT= IENT1, MIN (IENT2, NENT)
        CALL HGNF (ID, IENT, DAT(1,IENT), IERR)
        IF (IERR.NE.0) THEN
          IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNGET: error', IERR, ' reading entry',
     &                       IENT, ' from n-tuple', ID
          GOTO 999
        ENDIF
        NDAT= NDAT + 1
   10 CONTINUE
      IF (IENT2.GT.NENT) THEN
        IF (LUNERR.NE.0)
     &    WRITE (LUNERR,*) 'HNGET: n-tuple ID', ID, ' has only',
     &                     NENT, ' entries, not the requested',
     &                     IENT2
        IERR= -1
      ENDIF
*
  999 CONTINUE
      CALL HRESCD
      RETURN
      END
+DECK, HNFILL.
      SUBROUTINE HNFILL (LUN, ID, NENT, MXV, DAT, IERR)
***********************************************************************
*
*     HNFILL
*
*     Fill multiple events into the n-tuple.
*
*       LUN     Unit number used in previous HNOPEN
*       ID      The n-tuple identifier
*       NENT    Number of entries to write to end of n-tuple
*       MXV     Width (1st dimension) of DAT array.
*       DAT     Data in array (MXV,*)
*      *IERR    Error condition (0=OK)
*
*      1 Nov 91         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
+CDE, HNCLUN.
      INTEGER LUN, ID, NENT, IERR, MXV
+SELF, IF=IBMVM.
      REAL    DAT(MXV,NENT)
+SELF, IF=VAXVMS,ALPHAVMS.
*     The VAX will complain if the array size is given as zero,
*     unless we do the following.
*     For some crazy reason, the Alpha does not have MAX declared.
      INTEGER   MAX0
      INTRINSIC MAX0
      REAL    DAT(MXV,MAX0(NENT,1))
+SELF, IF=-IBMVM, IF=-VAXVMS, IF=-ALPHAVMS.
      REAL    DAT(MXV,*)
+SELF.
*
      INTEGER IENT
***********************************************************************
      IERR= 0
*
*     Check LUN is valid.
      IF (LUN.LE.0 .OR. LUN.GT.MAXLUN) THEN
        IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNFILL: invalid LUN:', LUN
        IERR= 99
        RETURN
      ENDIF
*
*     Save user's HBOOK/RZ directory.
      CALL HSAVCD
*
*     Set HBOOK/RZ directory.
      CALL HCDIR ('//'      // DIR(LUN), ' ')
      CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
*
      DO 10 IENT= 1, NENT
        CALL HFN (ID, DAT(1,IENT))
   10 CONTINUE
*
  999 CONTINUE
      CALL HRESCD
      RETURN
      END
+DECK, HNCLOS.
      SUBROUTINE HNCLOS (LUN, IERR)
***********************************************************************
*
*     HNCLOS
*
*     Closes n-tuple. This is vital if HNFILL is used.
*
*       LUN     Unit number used in previous HNOPEN
*      *IERR    Error condition (0=OK)
*
*      1 Nov 91         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
+CDE, HNCLUN.
      INTEGER LUN, IERR
*
      INTEGER ICYCLE
***********************************************************************
*     Check LUN is valid.
      IF (LUN.LE.0 .OR. LUN.GT.MAXLUN .OR. IOPEN(LUN).EQ.0) THEN
        IF (LUNERR.NE.0)
     &      WRITE (LUNERR,*) 'HNCLOS: invalid LUN:', LUN
        IERR= 99
        RETURN
      ENDIF
*
*     Save user's HBOOK/RZ directory.
      CALL HSAVCD
*
*     Set HBOOK/RZ directory.
      CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
      CALL HCDIR ('//'      // DIR(LUN), ' ')
*
*     Close n-tuple and file.
      IF (IOPEN(LUN).EQ.2) CALL HROUT (0, ICYCLE, ' ')
      CALL HREND (DIR(LUN))
*
*     The following is necessary (I believe) if we want to reopen the
*     file later. The HDELET removes the data in //PAWC/LUNnn.
      CLOSE (UNIT=LUN)
      CALL HCDIR ('//PAWC/' // DIR(LUN), ' ')
      CALL HDELET (0)
*
      IOPEN(LUN)= 0
*
      IERR= 0
      CALL HRESCD
      RETURN
      END
+DECK, HSAVCD.
      SUBROUTINE HSAVCD
***********************************************************************
*
*     HSAVCD
*
*     Utility routine to save current HBOOK/RZ directories.
*     Entry HRESCD will restore the previously saved directory.
*
*     22 Aug 90         Tim Adye
*
***********************************************************************
+SEQ, IMPNONE, T=PASS.
      CHARACTER*16 CWDIR0, CWDRZ0, CWDIR1, CWDRZ1
      SAVE         CWDIR0, CWDRZ0
***********************************************************************
      CWDIR0= ' '
      CWDRZ0= ' '
      CALL HCDIR  (CWDIR0, 'R')
      CALL RZCDIR (CWDRZ0, 'R')
+SELF, IF=HCDIR_DEBUG.
      WRITE (6,*)
     &    'HSAVCD: save HDIR= "',CWDIR0,      '", RZDIR= "',CWDRZ0,'"'
+SELF.
      RETURN
***********************************************************************
      ENTRY HRESCD
***********************************************************************
      CWDIR1= ' '
      CWDRZ1= ' '
      CALL HCDIR  (CWDIR1, 'R')
      CALL RZCDIR (CWDRZ1, 'R')
+SELF, IF=HCDIR_DEBUG.
      WRITE (6,*)
     &    'HRESCD:  was HDIR= "',CWDIR1,      '", RZDIR= "',CWDRZ1,'"'
+SELF.
      IF (CWDRZ0.NE.' ' .AND. CWDRZ0.NE.CWDRZ1 .AND.
     &    CWDRZ0.NE.CWDIR0) THEN
+SELF, IF=HCDIR_DEBUG.
        WRITE (6,*)
     &    'HRESCD:                                RZDIR->"',CWDRZ0,'"'
+SELF.
        CALL HCDIR (CWDRZ0, ' ')
      ENDIF
      IF (CWDIR0.NE.' ' .AND. CWDIR0.NE.CWDIR1) THEN
+SELF, IF=HCDIR_DEBUG.
        WRITE (6,*)
     &    'HRESCD:      HDIR->"',CWDIR0,'"'
+SELF.
        CALL HCDIR (CWDIR0, ' ')
      ENDIF
+SELF, IF=HCDIR_DEBUG.
      CALL HCDIR  (CWDIR1, 'R')
      CALL RZCDIR (CWDRZ1, 'R')
      WRITE (6,*)
     &    'HRESCD:  now HDIR= "',CWDIR1,      '", RZDIR= "',CWDRZ1,'"'
+SELF.
      RETURN
      END
+DECK, HGNPRM.
      SUBROUTINE HGNPRM (IDNT, NDIM, NEVENT, IERR)
***********************************************************************
*
*     HGNPRM
*
*     Utility routine to get the parameters for
*     the n-tuple IDNT into memory (using HGNPAR) and to return the
*     length (number of events) of the n-tuple. HGNPRM (or HGNPAR)
*     should be called before any calls to HGNF, execpt for a further
*     call to the same n-tuple.
*
*       IDNT    n-tuple id
*      *NDIM    N-tuple dimensionality (number of columns)
*      *NEVENT  Number of events (rows) in n-tuple
*      *IERR    Error condition (0=OK)
*
*     17 Aug 90         Tim Adye
*     22 Jan 92         Tim Adye: HBOOK v4.17 now has type declarations
*                                 in +KEEP, HCBOOK.
*     14 SEP 93         MIRIAM G: MAKE IT UNIX COMPATIPLE, USE STANDARD
*                                 HBOOK CALLS, NO MORE ZEBRA STUFF.
*
***********************************************************************
+SEQ, IMPNONETEST , T=PASS.
      INTEGER IDNT, NDIM, NEVENT, IERR, NVAR, NOENT, NVMAX
      PARAMETER (NVMAX=30)
      REAL RLOW(NVMAX),RHIGH(NVMAX)
      CHARACTER*80 CHTILT, CHTAG(NVMAX)
***********************************************************************
 
      NVAR=NVMAX
      CALL HGIVEN(IDNT,CHTILT,NVAR,CHTAG,RLOW,RHIGH)
      NDIM=NVAR
      CALL HNOENT(IDNT,NOENT)
      NEVENT=NOENT
      CALL HGNPAR(IDNT,'HGNPRM')
*
      IERR= 0
      RETURN
      END
